![Inea beloved](./logo.png "Kczi ddns")
# KCZI-DDNS

Simple program doing following:
1. Download your current ip
2. Lists all dns records inside cludflare zone
3. Updates dns record if necessary on conition that dns is commented in the right way

Start program in screen or as systemd servie or any other way. Add tag to dns record comment. Only dns records containing string ``tag``  from ``config.json`` will be updated. Generate token and paste into ``cloudflare_token.txt``. Enjoy

## Config

- cloudflare_token.txt - contains cloudflare token
- config.json - sets parameters. Example:
```json
{
    "zone_id": "bmV2ZXIgZ29ubmEgZ2l2ZSB5b3UgdXAgbmV2ZXIgZ29ubmEgbGV0IHlvdSBkb3duCg==",
    "tag": "automanaged",
    "ip_providers": [
        "https://ident.me"
    ],
    "update_interval": 120
}
```
Paths to config can be changed by env vars. Useful to launch multiple independent instances controling diffrent zones.
- ``TOKEN_PATH`` - sets path to cf token
- ``CONFIG_PATH`` - sets path to config
## Story

My beloved isp changed offer. Before I paid 5 pln monthy for static and public ip.  After changes I paing 10 pln monthy for public  non static ip. I wanted to automate updating dns records