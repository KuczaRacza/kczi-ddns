use std::env;
use tasks::Config;
use tokio;
mod tasks;
#[tokio::main]
async fn main() {
    let cf_token = std::fs::read_to_string(std::path::Path::new(
        &env::var("TOKEN_PATH").unwrap_or("cloudflare_token.txt".to_owned()),
    ))
    .unwrap();
    let config: Config = serde_json::from_str(
        std::fs::read_to_string(std::path::Path::new(
            &env::var("CONFIG_PATH").unwrap_or("config.json".to_owned()),
        ))
        .unwrap()
        .as_str(),
    )
    .unwrap();
    tasks::scan_task(config, cf_token).await;
}
