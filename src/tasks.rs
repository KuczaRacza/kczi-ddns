extern crate chrono;

use chrono::Local;
use reqwest::Request;
use std::{
    fmt::Display,
    time::{Duration, SystemTime},
};
use tokio::time::interval;

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct Config {
    pub zone_id: String,
    pub tag: String,
    pub ip_providers: Vec<String>,
    pub update_interval: u64,
}
pub async fn try_get_ip_form(ip_provider: &String) -> Result<String, reqwest::Error> {
    reqwest::get(ip_provider).await?.text().await
}
#[derive(serde::Serialize, serde::Deserialize, Debug, Clone)]
struct DnsRecord {
    #[serde(skip_serializing)]
    pub id: String,
    pub name: String,
    pub content: String,
    pub proxied: bool,
    pub comment: Option<String>,
    #[serde(rename = "type")]
    pub dns_type: String,
}
#[derive(serde::Serialize, serde::Deserialize, Debug)]
struct DnsResult {
    pub result: Vec<DnsRecord>,
}
enum CloudflareFetchError {
    ReqwestError(reqwest::Error),
    SeredereError(serde_json::Error),
}
impl From<reqwest::Error> for CloudflareFetchError {
    fn from(value: reqwest::Error) -> Self {
        CloudflareFetchError::ReqwestError(value)
    }
}
impl From<serde_json::Error> for CloudflareFetchError {
    fn from(value: serde_json::Error) -> Self {
        CloudflareFetchError::SeredereError(value)
    }
}
impl Display for CloudflareFetchError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::ReqwestError(err) => f.write_str(err.to_string().as_str()),
            Self::SeredereError(err) => f.write_str(err.to_string().as_str()),
        }
    }
}
async fn get_cloudflare_dns(
    token: &String,
    zone: &String,
    client: &mut reqwest::Client,
) -> Result<DnsResult, CloudflareFetchError> {
    Ok(serde_json::from_str::<DnsResult>(
        client
            .get(format!(
                "https://api.cloudflare.com/client/v4/zones/{}/dns_records",
                zone
            ))
            .bearer_auth(token)
            .header("Content-Type", "application/json")
            .send()
            .await?
            .text()
            .await?
            .as_str(),
    )?)
}
fn alter_record<'a>(mut dns_record: DnsRecord, new_ip: &String, tag: &String) -> Option<DnsRecord> {
    if let Some(comment) = &dns_record.comment {
        if comment.contains(tag) && dns_record.content != *new_ip {
            println!(
                "[{}] Updating {} from {} to {}",
                Local::now(),
                &dns_record.name,
                &dns_record.content,
                &new_ip
            );
            dns_record.content = new_ip.clone();
            Some(dns_record)
        } else {
            None
        }
    } else {
        None
    }
}
async fn send_change(
    dns: &DnsRecord,
    zone: &String,
    token: &String,
    client: &mut reqwest::Client,
) -> Result<(), CloudflareFetchError> {
    client
        .patch(format!(
            "https://api.cloudflare.com/client/v4/zones/{}/dns_records/{}",
            zone, dns.id
        ))
        .bearer_auth(token)
        .header("Content-Type", "application/json")
        .body(serde_json::to_string(dns)?)
        .send()
        .await
        .and_then(|_res| Ok(()))
        .map_err(|err| err.into())
}
async fn get_ip(config: &Config) -> Result<String, ()> {
    for ip_provider in &config.ip_providers {
        match try_get_ip_form(ip_provider).await {
            Ok(ip) => {
                return Ok(ip);
            }
            Err(err) => eprintln!(
                "[{}] Cannot optain ip from {} . Reason: {}",
                Local::now(),
                ip_provider,
                err.to_string()
            ),
        }
    }
    return Err(());
}
async fn update_dns_with_ip(
    ip: String,
    config: &Config,
    token: &String,
    client: &mut reqwest::Client,
) -> Result<(), CloudflareFetchError> {
    let dns_list = get_cloudflare_dns(&token, &config.zone_id, client).await?;
    let to_update: Vec<DnsRecord> = dns_list
        .result
        .into_iter()
        .filter_map(|dns| alter_record(dns, &ip, &config.tag))
        .collect();
    for dns in to_update {
        send_change(&dns, &config.zone_id, token, client).await?;
    }
    Ok(())
}
pub async fn scan_task(config: Config, token: String) {
    let mut wait = interval(Duration::from_secs(config.update_interval));
    let mut client = reqwest::Client::new();
    loop {
        if let Ok(my_ip) = get_ip(&config).await {
            if let Err(err) = update_dns_with_ip(my_ip, &config, &token, &mut client).await {
                eprintln!(
                    "[{}] Cannot update dns ip. Reason {}",
                    Local::now(),
                    err.to_string()
                )
            }
        } else {
            eprintln!("[{}] Cannot optain ip", Local::now())
        }

        wait.tick().await;
    }
}
